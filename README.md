Sit Ski 
=========

This gitlab page is used solely for communicating our project progress and overview to our
clients, stakeholders, and auditors. Our file storage system, which utilises dropbox, can 
be found here:

https://www.dropbox.com/sh/nka3wpekwmydce3/AACAp05KA3jkRc4qqmQK8f1ra?d

Table of Contents
=================
  * [Project Update - Audit Edition](#project-update)
      * [Outputs](#outputs)
      * [Decisions](#decisions)
      * [Team](#team)
      * [Communications](#communications)
      * [Reflection](#reflection)

  * [Project Introduction](#project-introduction)
      * [Descriptive Overview](#descriptive-overview)
      * [Current State of Project](#current-state-of-project)
      * [Project Scope](#project-scope)

  * [Project Team](#project-team)
  
  * [Project Requirements](#project-requirements)
  
  * [Project Timeline](#project-timeline)
      * [Milestones](#milestones)
      * [Gantt Chart](#gantt-chart)

  * [Administration and Tools](#administration-and-tools)
      * [Weekly Meeting Time](#weekly-meeting-time)
      * [Resources and Materials](#resources-and-materials)
      * [Funding](#funding)
      * [Repository Strategy](#repository-strategy)
      * [Tools to Use](#tools-to-use)

## Project Update - Audit Edition 

Previous project updates in word format can be found here:

https://www.dropbox.com/sh/qf2p88bgzzoggze/AABTXo6PLPMXi6TbZEvc9Gi3a?dl=0

This is updated every Friday. 

The powerpoint used during our Week 10 Audit presentation can be found here:

https://www.dropbox.com/sh/90zaayk3218c65o/AACE6kweYKVCpGgpkLNm14mga?dl=0

### Output

The past week, the team focused on deliverables required for the showcase on Tuesday 
the 8th of May. As the previous goal of having a wearable prototype ready by the 
showcase was now unachievable, we focused on ways to demonstrate our design. This 
was done by making the design the central piece on our poster with a detailed breakdown 
of all aspects of it. We have also 3D printed ¼ scale models of our design for people 
to interact with at the showcase.

We also created 1/4 scale moulds and tested our vacuum forming process, which was a 
success. This was done use HIPS, which differs from the final material choice, but 
prooves the manufacturing process selected is viable.

We also developed a simple website for people to easily see the key points of the design 
through a a few simple images and a video showing an explosion of the design. The website
can be seen at the following link:

https://desilvashohan.wixsite.com/sitski

### Decisions

The decision was made this past week to focus on our audit and showcase deliverables, 
and to resume our main protype manufacturing once this, and other high intensity uni 
tasks amongst the team, have passed. This does not impact the overall deliverability 
to the client as the hard deadline for a prototype to Sam is the 20th of June, which 
the team is still on track to meet. 

Our realisation that we would not be able to deliver a wearable prototype by the showcase
meant we had to develop a novel way of demonstrating our design, which was complete. 
This was done by developing the quaster size model, as mentioned above. 

### Team

This week the team split into three groups of 2 to tackle each of the deliverables 
required. Meison and Sho took to solidworks to design the moulds and 3D print scaled
models. This involved slight adjustements to the design to ensure manufacturability 
and importing them into 3D printing code software. 

Kris and Dave were in charge of creating the proof of concept HIPS models. They also 
continued contacting suppliers and businessed for quotes on manufacturing.

Alex and Jess were the primary designers of the poster, including layout, content, 
and design scheme. This included mocking up the initial concept design and then acting 
on feedback from the pre audit tutorial.

### Communications

A client meeting was scheduled with Sam to take place on Wednesday, but unfortunately 
due to car issues this did not take place. Due to this, a project update was compiled 
and given to Sam and we are awaiting his feedback. 

Other communication was done with external entities, suppliers, and manufacturers by 
Kris and Dave, seeking advice and quotes for upcoming prototyping needs. 

Meison and Shohan also put together documentation for the CAD design process, detailing 
the decisions made along the way, and how the final design came to be. This is split 
across two documents, one is summary in word format, the other is a decision log in 
excel.

### Reflection

This week we received a lot of feedback from our shadow group and Chris Brown for our
showcase poster. A lot of this feedback we found very useful and incorporated it in
our final poster design. This included remarks about the overall layout and what 
particular areas needed more emphasis. 

## Project Introduction

### Descriptive Overview

The client for the project is Sam Tait, a Paralympic sit-skier. He competes in five 
events (Downhill, Super G, Slalom, Giant Slalom and Super combined) in which he weaves 
through gates. To get the fastest time, competitors regularly hit their legs on the 
plastic gates in order to make the turns faster. Without protection, this can cause 
bruising and internal damage to the legs. The goal of this project is to provide Sam 
with a leg cover/guard which he feels comfortable wearing for all five of his events. 
This cover cannot impact upon his performance in any way. 

### Current State of Project

In the winter semester of 2017, a project group commenced the project. They designed 
and manufactured a cover which Sam will use during the PyeongChang Paralympics. The 
main issue with the current cover is that the edges can catch the snow, slowing down 
Sam’s performance and potentially causing a crash. Based on this problem, the future 
developments for the project is to refine the design of the cover to prevent these 
issues, and then manufacture the cover to be used in professional competition.

### Project Scope

The scope for the project is to create and confirm a final design for the sit-ski cover. 
This cover should be comfortable to use for as many of the client’s events as possible 
(not necessarily performance optimisation but feels comfortable to wear for all races). 
The final manufacturing of the cover is not within the scope, however it will require 
prototyping and testing to determine the success of the design. This scope was established 
by the project team and the previous team (acting in lieu of the client).

## Project Team

The main project team consists of 6 engineering students. The team also includes 
the client and the previous team. The previous team will act as a guide for the team 
as well as the client until Sam has completed his Olympic competition.

+	Alex Cardew-Hall (u5560584)
+	Shohan de Silva (u5565229)
+	Kris Erb (u5605365)
+	Meison Wallace (u5826375)
+	Jessica Kennedy (u5798451)
+	David Hutchinson (u5365630)
+	Sam Tait (Client)
+	Redg Mathur, James Spollard, and Nathan Pereira (Previous team and stakeholders)

## Project Requirements

Building from the previous work on this design, new requirements have been made for this 
next phase of the project. These are seen below.

| ID    | Requirement     | Reasoning      |
| ----- | --------------- | -------------- |
| 2.1   | To minimise the width of the enclosure | The enclosure does not hit the snow on tight turns, causing loss of speed, or possible crashes   |
| 2.2   | To be able to use the enclosure on all 5 race types    | A singular device can be used instead of several   | 
| 2.3   | Funding sourced or low cost development   | Previous funding has been exhausted so new funding is required or costs have to be minimised  | 
| 2.4   | To have a final product finalised by September / October, 2018 | It can be used in the 2018/2019 snow season overseas |

The previous set of requirements, made by the previous team which still need to be adhered to.
Our full requirements analysis can be found in our dropbox repository ( 02. Design > 01. Requirements )

## Project Timeline
### Milestones

Below are some key milestones identified by the team and client. These have been 
identified to ensure the project can be completed on time. Milestones align with key 
reporting dates. Whilst delays have occured we still expect to meet our key milestones.
+	5-7th March – Project Audit 1 – Commence design phase
+	26-28th March – Project Audit 2 
+	8th April – Initial designs complete for stakeholder review. Design selection should occur at this point (note that this milestone has been pushed back).
+	8th-9th April – Commence prototyping phase (note that this milestone has been pushed back).
+	4th May – Project poster due
+	8th May – Wearable prototype required for showcase
+	28th May – Final design confirmed

### Gantt Chart
A gantt chart detailing the project timeline can be found in our dropbox respository. 
( 01. Admin > 04. Project Timeline > ENGN4221 Prelim Gantt Chart )

## Administration and Tools

### Weekly Meeting Time

Time has been allocated throughout the week for compulsory team meetings. The purpose 
of the meetings is for the project team to discuss how the project is progressing, assign work, 
and prepare for upcoming tutorials and project audits. These will take place every week on 
Wednesday 13:00-14:00 in Hancock 3.29, with Thursday 11:00-13:00 in Hancock 3.28 available 
should the project team require additional face-to-face time.

### Resources and Materials

Given that the scope of the project is predominantly design, the primary resources 
will be design software. Access to required software is provided by the ANU.
The project team has inherited some basic manufacturing materials such as some rolls 
of fibre-glass. Further materials may need to be sourced for prototyping and will be 
confirmed when the design phase is complete. Materials for each design are currently
being investigated.


### Funding

Work is undersay on preparing a detailed budget to hand to ANU/Chris for initial 
funding. We do not expect this value to exceed $1000 to allow for all of our prototyping 
needs

### Repository Strategy

The team will work primarily using Dropbox for file storage. The Dropbox is broken down 
into various overarching folders (including administration, conceptual design, prototyping 
and testing, resources etc.), from which further subfolders exist.

This repository area can be found by using the URL below.

https://www.dropbox.com/sh/nka3wpekwmydce3/AACAp05KA3jkRc4qqmQK8f1ra?dl=0

For the previous groups work on this project, their repository can be found here:

https://drive.google.com/drive/folders/0B8sIvKiIaXXqaFN5VXNuSUlRYUU

### Tools to use
+ Admin
    + Dropbox
    + Github
+ Design
    + Solidworks (Campus)
    + ANSYS (ANU Campus)
    + Meshlab (Opensource Software)
    + Autodesk MeshMixer (Free to use)
+ Manufacturing and Testing
    + ANU Workshop and Maker Space   
    + UC design workshop
    + TBC once prototyping commences


